org 0x100

%define T_NIL     0
%define T_NODE    1
%define T_SYMBOL  2
%define T_NUMBER  3

    ; ASCII ranges (num: '0' - '9', sym0: '!' - '\'', sym1: '*' - '}')
%define NUMLOW    48
%define NUMDELTA  (57 - NUMLOW)
%define SYMDELTA0 (39 - 33)
%define SYMDELTA1 (125 - 42)

;    jmp main


; === 83 bytes total ===
tokenize:                       ; al=char, bl=mode, cx=storage
    lodsb                       ; (1) char = *string
    ; Range check for numbers
    sub al, NUMLOW              ; (2) char -= NUMLOW
    cmp al, NUMDELTA            ; (2) char == (NUMHIGH - NUMLOW) ?
    ja .symbols                 ; (2) (= 7 bytes)
    ; We have a digit, so add it to the stored number
.numbers:
    mov bl, T_NUMBER            ; (2) mode = NUMBER
    imul cx, 10                 ; (3) storage *= 10
    add cx, ax                  ; (2) storage += char
    ret                         ; (1) (= 8 bytes)
.symbols:
    ; Range check for symbols
    add al, 6                   ; (2) char += (SYMLOW0 - NUMLOW)
    cmp al, SYMDELTA1           ; (2) char == (SYMHIGH0 - SYMLOW0) ?
    jb .hash                    ; (2)
    add al, 9                   ; (2) char += (SYMLOW1 - ...)
    cmp al, SYMDELTA0           ; (2) char == (SYMHIGH1 - SYMLOW1)
    ja .other                   ; (2) (= 12 bytes)
.hash:
    ; We have a symbol, grab the orignal character from si and hash it
    mov bl, T_SYMBOL            ; (2) mode = SYMBOL
    dec si                      ; (2) string--
    lodsb                       ; (1) char = *string++
    add cx, ax                  ; (2) storage += char (ah MUST BE ZERO)
    and cx, 0xFF                ; (4) storage &= 0xFF
    ret                         ; (1) (= 12 bytes)
.other:
    ; It's not a symbol or a number
    ; So if the mode is SYMBOL, finish the hash and call the parser to insert it
    cmp bl, T_SYMBOL            ; (3) mode == T_SYMBOL ?
    jne .other_num              ; (2)
    xor cx, 0xFF                ; (3) storage ^= 0xFF
    inc cx                      ; (2) storage++
    and cx, 0xFF                ; (4) storage &= 0xFF
    call print                  ; (3)
    jmp .finish                 ; (2) (= 19 bytes)
.other_num:
    ; If the mode is a NUMBER, call the parser to insert it
    cmp bl, T_NUMBER            ; (3) mode == T_NUMBER ?
    jne .finish                 ; (2)
    call print                  ; (3) (= 8 bytes)
.finish:
    ; Bit finicky, but it's either a brace or we dont care about it
    ; Grab the original char and subtract by '('.
    ; Thereby setting mode to 0 for Open Brace, 1 for Close Brace
    xor cx, cx                  ; (2) storage = 0
    dec si                      ; (2) string--
    lodsb                       ; (1) char = *string++
    sub al, '('                 ; (2)
    cmp al, 2                   ; (2) char > 2 ?
    ja .end                     ; (2)
    mov bl, al                  ; (2) mode = char
    call print                  ; (3) (= 16 bytes)
.end:
    xor bl, bl                  ; (2) mode = 0
    ret                         ; (1) (= 3 bytes)

print:                          ; bh=mode,cx=storage
    mov dx, bx
    add dx, 'A'
    mov ah, 02h
    int 21h
    xor ax, ax
    ret

main:
    mov si, string
    xor bx, bx
    call tokenize
    call tokenize
    call tokenize
    call tokenize
    call tokenize
    call tokenize
    call tokenize
    call tokenize
    call tokenize
    ret

section .data
string: db '(A (B C))'
